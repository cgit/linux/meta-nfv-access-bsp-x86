FILESEXTRAPATHS_prepend := "${THISDIR}/linux-intel:"

require recipes-kernel/linux/linux-intel_5.10.bb
require recipes-kernel/linux/linux-deploy-kconfig.inc
require recipes-kernel/linux/linux-common-config.inc

# Note: these should be removed to bump kernel back to v5.10.78
SRC_URI_remove = " \
    file://0001-io-mapping-Cleanup-atomic-iomap.patch \
"

LINUX_VERSION = "5.10.35"
SRCREV_machine = "0fd9e7f81ac665d77c40d5c4a3815e46d4d07e90"
SRCREV_meta = "cd049697e9b2d3e9118110d476075ff8f87202cc"

SRCREV_metaenea = "${AUTOREV}"
SRC_URI_append = " git://${COREBASE}/../src/enea-kernel-cache;protocol=file;usehead=1;type=kmeta;name=metaenea;destsuffix=enea-kernel-meta"

do_kernel_metadata_prepend() {
    ## ENEA_start ##
    # kernel-meta patching via patch files added to SRC_URI is currently broken, handle it here

    # Upstream BSPs include aufs support (by patching the kernel source + enabling it in kernel config).
    # However, our current kernel version contains a backported commit that conflicts with aufs patches.
    # Until aufs patches are rebased in yocto-kernel-cache, disable aufs kernel source patching, since
    # we don't enable it via kernel config anyway.
    sed -i -E 's/^(include features.aufs.*)$/# \1/g' ${WORKDIR}/kernel-meta/ktypes/standard/standard.scc

    # Upstream yocto-kernel-cache adds a backported patch that meanwhile got upstreamed, but was not
    # yet removed, causing patching conflicts. Until upstream cleans up this mess, handle it here.
    sed -i -E 's/^(patch cgroup1-fix-leaked.*)$/# \1/g' ${WORKDIR}/kernel-meta/patches/misc/misc.scc
    ## ENEA_end ##
}

# Allow BSPs to supply patches without explicitly adding the scc files to SRC_URI/KERNEL_FEATURES
KMETA_EXTERNAL_BSPS = "t"

# KERNEL CONFIG DEFAULT CHECK LEVELS:
#   0: no reporting
#   1: report options that are specified, but not in the final config
#   2: report options that are not hardware related, but set by a BSP
# KCONF_AUDIT_LEVEL="1"
# KCONF_BSP_AUDIT_LEVEL="2"
# KMETA_AUDIT="yes"
# KMETA_AUDIT_WERROR=""

KERNEL_FEATURES_append = " bsp/enea-common-bsp/enea-common-bsp.scc"

KERNEL_FEATURES_append = " features/apic/x2apic_y.scc"

KERNEL_FEATURES_append = " features/udev/udev.scc"

# NFS boot support
KERNEL_FEATURES_append = " features/blkdev/net_blk_dev.scc"

# Ramdisk boot support
KERNEL_FEATURES_append = " features/blkdev/ramdisk_blk_dev.scc"

# Intel 10G ports(SoC)
KERNEL_FEATURES_append_x86-64 = " features/dca/dca_y.scc"

# NMVe SSD
KERNEL_FEATURES_append = " features/nvme/nvme.scc"

# IPMI support
KERNEL_FEATURES_append = " features/ipmi/ipmi.scc"
KERNEL_FEATURES_append = " features/ipmi/ipmi_ssif.scc"

# ACPI Tiny power button
KERNEL_FEATURES_append = " features/acpi/acpi_tiny_pwrbtn_y.scc"

# CPULIST: Allow using "63" as an alias for "last" in nohz_full, isolcpus etc.
KERNEL_FEATURES_append = " features/cpulist_abbrev/cpulist_abbrev_enea.scc"

# IPv4 waiting for carrier on
KERNEL_FEATURES_append = " patches/ipv4/ipv4wait.scc"

# Fixes for pci
KERNEL_FEATURES_append = " patches/drivers/pci/quirks.scc"
